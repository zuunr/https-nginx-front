# https-nginx-front
The https-nginx-front is a reverse proxy docker container that terminates the incoming HTTPS connection before passing it down to mapped applications.

It supports automatic renewal of certificates by using the Let's Encrypt certbot tool.

## Configuration

The module requires both environment variable configuration as well as changes to the configuration files.

#### Environment variable

The environment variable **CERTBOT_MODE** changes the way the certbot is run. There are three modes:

1. CERTBOT_MODE is set to *PROD*: The certbot will create real certificates and update them every 12h hour.
2. CERTBOT_MODE is set to *STAGING*: The certbot will create staging certificates an update them every 12 hour. This should be the main testing mode.
3. CERTBOT_MODE is not set, or does not match above: The certbot will be disabled, and the server will function with the provided self signed certs.

#### Update Dockerfile

First we are going to update the **ENV** variables in the **Dockerfile**, replacing your@email.com and www.yoursite.com,yoursite.com with actual values.

The *REGISTER_EMAIL* is used as input to the --email parameter in the certbot script.

The *DOMAINS* is a comma separated domain list, i.e. you can add multiple domains that you want to point to your nginx server.

```dockerfile
ENV REGISTER_EMAIL your@email.com
ENV DOMAINS www.yoursite.com,yoursite.com
```

#### Update nginx.conf

In the next step we are going to update the **nginx.conf**, replacing the two *server_name* **yoursite.com** with your domain name.

```nginx
listen              80;
server_name         yoursite.com;
...
listen              443;
server_name         yoursite.com;
```



We also need to add a new location for your service endpoints in the **nginx.conf** with the correct *proxy_pass* IP address pointing to your app's IP address.

```nginx
location /v1/my-services/api {
    proxy_set_header    Host $host;
    proxy_set_header    X-Real-IP $remote_addr;
    proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Host $server_name;
    proxy_set_header    X-Forwarded-Proto $scheme;

    proxy_pass          http://10.0.0.0:8080;
}
```



To use the module in front of a Kubernetes service, locate your service's ClusterIP (if the nginx module is deployed in the same cluster).

Connect to Kubernetes, either locally or in cloud environment shell, and issue the following command to view the IPs.

```shell
kubectl get services

# Displays the IPs
NAME         TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP      10.0.0.1       <none>        443/TCP          6d
my-services  ClusterIP      10.0.123.123   <none>        8080:30713/TCP   6d
```

#### Register without email

If you would like to register your domain without providing an email you need to update the **init_certbot.sh** and **init_certbot_staging.sh** scripts. Replace --email $REGISTER_EMAIL with --register-unsafely-without-email.

```sh
#init_certbot.sh
certbot certonly --webroot -w /usr/share/nginx/html --register-unsafely-without-email --non-interactive --agree-tos --cert-name generatedcert --domains $DOMAINS

#init_certbot_staging.sh
certbot certonly --webroot -w /usr/share/nginx/html --register-unsafely-without-email --non-interactive --agree-tos --staging --cert-name generatedcert --domains $DOMAINS
```

## Build docker image

To build the docker image, issue the following command (replace *zuunr/https-nginx-front* with your image name).

```bash
docker build -t zuunr/https-nginx-front:myVersion .
```

## Run docker image
To run the image locally, issue the following command (replace *zuunr/https-nginx-front* with your image name).

```bash
docker run -it --rm -p 443:443 -p 80:80 -e CERTBOT_MODE=STAGING zuunr/https-nginx-front
```

The image requires both 443 and 80 to be open for automatic certificate creation. Remember to configure the CERTBOT_MODE environment variable correctly.

The --rm will destroy the container after exit.

## Let's encrypt

Remember that Let's Encrypt has a rate limit, so set the CERTBOT_MODE environment variable to **STAGING** to test the chain before generating real certs (destroy the container after you have tested with staging).

A reminder, destroying the container will destroy the certs so be careful how many times you destroy the container if you are creating "real certs", so you are not hitting the rate limit. Starting and stopping the container however is fine.

## Avoid re-creating certs

If you want to avoid creating new certs whenever the container is destroyed, mount a shared volume to your docker container so that the */etc/letsencrypt* is shared.